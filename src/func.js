const getSum = (str1, str2) => {
  let num1 = str1
  let num2 = str2
  num1 = num1 === '' ? '0' : num1
  num2 = num2 === '' ? '0' : num2
  const regexp = /^\d+$/
  if (
    typeof num1 !== 'string' ||
    typeof num2 !== 'string' ||
    isNaN(parseInt(num1)) ||
    isNaN(parseInt(num2)) ||
    !regexp.test(num1) ||
    !regexp.test(num2)
  ) {
    return false
  }

  return (parseInt(num1) + parseInt(num2)).toString()
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsCount = 0
  let commentsCount = 0

  listOfPosts.forEach(({ author, comments }) => {
    if (author === authorName) {
      postsCount += 1
    }
    if (comments) {
      comments.forEach(({ author }) => {
        if (author === authorName) {
          commentsCount += 1
        }
      })
    }
  })
  return `Post:${postsCount},comments:${commentsCount}`
}

const tickets = (people) => {
  let cash = 0
  const money = people.map((item) => parseInt(item.toString()))

  for (const bill of money) {
    if (bill > 25) {
      if (bill - cash > 25) {
        return 'NO'
      }
      cash -= bill - 25
    }
    cash += 25
  }
  return 'YES'
}

module.exports = { getSum, getQuantityPostsByAuthor, tickets }
